#ifndef ADCS_H
#define ADCS_H

#include <stdint.h>

// Binary
#define ACD_BIN "ac-d"

// UDP
#define ACD_RPC_SOCKET UINT16_C(50200)

//==============================================================================
/*
  Whenever we want to send commands to ADCS
 */

#define ADCS_ARG_TYPE_RELATIVE 0x01
#define ADCS_ARG_TYPE_ABSOLUTE 0x02
#define ADCS_ARG_TYPE_TIMESTAMP 0x03

//==============================================================================
/**
 * From ADCS to hm_d
 * Requested through ADCS_REQUEST_PACKET, contains ADCS_RESPONSE_PACKET
 * ADCS_RESPONSE_PACKET contains type number of parent struct
 */

/*
  ADCS_RAW_HEALTH_PACKET is a summary of how ADCS interprets the values in a
  reasonable way. Quaternions for attitude and other derived units are here
 */

#define ADCS_DETERMINATION_LEFFERT 0x1
#define ADCS_DETERMINATION_PSIAKI 0x2

// NONE can be used to turn off torque coils
#define ADCS_CONTROL_NONE 0x00
#define ADCS_CONTROL_BCROSS 0x01
#define ADCS_CONTROL_QRF 0x02

_Pragma("GCC diagnostic push")
_Pragma("GCC diagnostic ignored \"-Wattributes\"")
typedef struct __attribute__((packed)) {
	uint64_t absolute_pos;
	uint64_t relative_pos;
	double attitudeEstimate[4];
	double attitudeTarget[4];
	double attitudeError[4];
	double rateEstimate[3];
	double rateTarget[3];
	double orbitalPosEstimate[3];
    uint8_t cur_det;
    uint8_t cur_control;
	uint64_t time;
} ADCS_HEALTH_PACKET;
_Pragma("GCC diagnostic pop")

#define ADCS_OP_GET_HEALTH 1
#define ADCS_OP_SET_CONTROLS 2 // 1 for enabled, 0 for disabled, defaults to 1
#define ADCS_OP_SET_DET 3

#define ADCS_OP_WAIT (10*1000*1000)

// 5, 5, and 5 
#define ADCS_OP(pos___, type___, var___) \
    (((ADCS_OP_SENSOR_POS_ ## pos___ << 0) | \
      (ADCS_OP_SENSOR_TYPE_ ## type___ << 5) | \
      (ADCS_OP_SENSOR_VAR_ ## var___ << 10)) << 2)

// All position returns an array with everything
#define ADCS_OP_SENSOR_POS_ALL 0U

#define ADCS_OP_SENSOR_POS_PLUS_X 1U
#define ADCS_OP_SENSOR_POS_MINUS_X 2U
#define ADCS_OP_SENSOR_POS_PLUS_X_2 3U
#define ADCS_OP_SENSOR_POS_MINUS_X_2 4U

#define ADCS_OP_SENSOR_POS_PLUS_Y 5U
#define ADCS_OP_SENSOR_POS_MINUS_Y 6U
#define ADCS_OP_SENSOR_POS_PLUS_Y_2 7U
#define ADCS_OP_SENSOR_POS_MINUS_Y_2 8U

#define ADCS_OP_SENSOR_POS_PLUS_Z 9U
#define ADCS_OP_SENSOR_POS_MINUS_Z 10U
#define ADCS_OP_SENSOR_POS_PLUS_Z_2 11U
#define ADCS_OP_SENSOR_POS_MINUS_Z_2 12U

#define ADCS_OP_SENSOR_TYPE_MT 1U
#define ADCS_OP_SENSOR_TYPE_GY 2U
#define ADCS_OP_SENSOR_TYPE_FC 3U
#define ADCS_OP_SENSOR_TYPE_TC 4U

#define ADCS_OP_SENSOR_VAR_TEMP 1U
#define ADCS_OP_SENSOR_VAR_DPS 2U
#define ADCS_OP_SENSOR_VAR_MAG 3U

#define ADCS_OP_SENSOR_VAR_UV_LIGHT 8U
#define ADCS_OP_SENSOR_VAR_VIS_LIGHT 9U
#define ADCS_OP_SENSOR_VAR_IR_LIGHT 10U

#define ADCS_OP_SENSOR_VAR_CURRENT 11U
#define ADCS_OP_SENSOR_VAR_VOLTAGE 12U

#endif
